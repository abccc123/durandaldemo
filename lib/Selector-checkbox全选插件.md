# Selector模块使用说明

## 简介

此模块是基于ko编写的Selector选择器的构造函数模块。

此模块支属性 `isSelectAll`、`selectedCount`、`selectedList`、`list`,同时支持操作 `selectAll`、`unselectAll`、`invertSelect`操作

## 模块位置

    app/component/selector/Selector

## 使用示例和说明

由于是基于ko编写，所以传入构造函数的list必须是`observableArray`监控数组，数组中的每一项必须有一个`observable`属性`isSelected`,表示是否选择,默认是`false`,只需要按照如下面的`views`进行相关的绑定，就不必关心之间的交互

**viewmodel**

```javascript
define(['knockout'], function (ko) {
    var list = ko.observableArray([
        {
            name:'item1',
            isSelected:ko.observable(false)
        }
        ]);
    var selector = new Selector(list);

    return  {
        selector: selector,
        onBtnSelectedAllClick: function() {
            selector.selectAll();
        },
        onBtnUnselectAllClick: function() {
            selector.unselectAll();
        },
        onBtnInvertSelectClick: function() {
            selector.invertSelect();
        }
    };
}
```

**view**

```html
<input type="checkbox" data-bind="checked:selector.isSelectAll" />
<button type="button" data-bind="click:onBtnSelectedAllClick">全选</button>
<button type="button" data-bind="click:onBtnUnselectAllClick">全不选</button>
<button type="button" data-bind="click:onBtnInvertSelectClick">反选</button>

<div class="list-tip">总共<span data-bind="text:selector.list().length" >0</span>项，当前选中了 <span data-bind="text:selector.selectedCount" >0</span> 项</div>

<ul data-bind="foreach:selector.list">
    <li>
        <div class="list-item">
            <input type="checkbox" data-bind="checked:isSelected" />
            <span class="item-value" data-bind="text:name"></span>
        </div>
    </li>
</ul>

```


## 属性

1. **list** - 即传入Selector的list,表示总的数据列表
1. **selectedList** - 已选中列表，表示从list中选中的数据列表
1. **selectedCount** - 表示选中的数量
1. **isSelectAll** - 获取是否进行了全选操作

## 方法

1. **selectAll** - 进行全选操作
1. **unselectAll** - 进行取消全选操作
1. **invertSelect** - 进行反选操作