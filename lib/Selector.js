// / * description 此模块为选择器的构造函数模块，支持如下属性 isSelectAll/selectedCount/selectedList/list,同时支持全选/全不选/反选操作
//  * 
//  */
define(['knockout'], function (ko) {
    function Selector(list) {
        var self = this;
        self.list = list;
        //判断是否是全选操作同时进行对应的显示
        self.isSelectAll = ko.pureComputed({
            read: function () {
                return (this.selectedCount() === this.list().length) && (this.list().length !== 0);
            },
            write: function (value) {
                this.beforeSelectAllChange(value);
                if (value) {
                    this.selectAll();
                } else {
                    this.unselectAll();
                }
                this.afterSelectAllChanged(value);
            },
            owner: self
        });

        //选中的数量
        self.selectedCount = ko.pureComputed(function () {
            return this.selectedList().length;
        }, self);
        //选中项列表
        self.selectedList = ko.pureComputed(function () {
            var list = [];
            var i = 0;
            var len = this.list().length;
            for (i = 0; i < len; i++) {
                var obj = this.list()[i];
                if (obj.isSelected()) {
                    list.push(obj);
                }
            }
            return list;
        }, self);
    }
    
    //反选操作
    Selector.prototype.invertSelect = function () {
        var i = 0;
        var len = this.list().length;
        for (; i < len; i++) {
            var o = this.list()[i];
            o.isSelected(!o.isSelected());
        }
    };
    //全选
    Selector.prototype.selectAll = function () {
        var i = 0;
        var len = this.list().length;
        for (; i < len; i++) {
            this.list()[i].isSelected(true);
        }
    }
    //取消全选
    Selector.prototype.unselectAll = function () {
        var i = 0;
        var len = this.list().length;
        for (; i < len; i++) {
            this.list()[i].isSelected(false);
        }
    }
    /***
     * 全选计算执行之前
     */
    Selector.prototype.beforeSelectAllChange=function(isSelectAll){}
    /**
     * 全选计算执行之后
     * @param {*} isSelectAll 
     */
    Selector.prototype.afterSelectAllChanged=function(isSelectAll){}

    return Selector;
});