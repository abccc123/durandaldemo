﻿/*
 * @Author: your name
 * @Date: 2021-09-24 14:27:41
 * @LastEditTime: 2021-09-26 09:41:24
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \DurandalDemo\app\modal\index.js
 */
define(['durandal/app', './customModal', 'plugins/dialog'], function (app, CustomModal,bootstrap) {
    return {
        showCustomModal: function() {
            CustomModal.show().then(function(response) {
                app.showMessage('You answered "' + response + '".');
            });
        },
        
        showbootstrap: function() {
            // bootstrap.showBootstrapModal(item, {});
            bootstrap.showBootstrapMessage('Are you sure you want to leave this page?', 'Navigate', ['Yes', 'No'], null);
        }
    };
});