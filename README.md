![Durandal](http://durandaljs.com/media/DURANDAL-FINAL-HI-RES-LOGO-HOR-WEB.png)

Durandal is a cross-device, cross-platform client framework written in JavaScript and designed to make Single Page Applications (SPAs) easy to create and maintain. We've used it to build apps for PC, Mac, Linux, iOS and Android...and now it's your turn...

[![Build Status](https://travis-ci.org/BlueSpire/Durandal.png?branch=master)](https://travis-ci.org/BlueSpire/Durandal)

## Features

* Clean MV* Architecture
* JS & HTML Modularity
* Simple App Lifecycle
* Eventing, Modals, Message Boxes, etc.
* Navigation & Screen State Management
* Consistent Async Programming w/ Promises
* App Bundling and Optimization
* Use any Backend Technology
* Built on top of [jQuery](http://jquery.com/), [Knockout](http://knockoutjs.com/) & [RequireJS](http://requirejs.org/).
* Integrates with popular CSS libraries such as [Bootstrap](http://getbootstrap.com/) and [Foundation](http://foundation.zurb.com/).
* Make your own templatable and data-bindable widgets.
* Fully Testable

### Combined Lifecycle
http://durandaljs.com/documentation/Hooking-Lifecycle-Callbacks.html
<table class="table table-bordered">
  <tbody><tr>
    <th>Callback</th>
    <th>Lifecycle</th>
    <th>Purpose</th>
  </tr>

  <tr class="success">
    <td class="success"><code>getView()</code> and <code>viewUrl</code></td>
    <td>Composition</td>
    <td>Enables the new object to return a custom view.</td>
  </tr>

  <tr>
    <td><code>canDeactivate()</code></td>
    <td>Activator</td>
    <td>Allows the previous object to cancel deactivation.</td>
  </tr>

  <tr>
    <td><code>canActivate()</code></td>
    <td>Activator</td>
    <td>Allows the new object to cancel activation.</td>
  </tr>

  <tr>
    <td><code>deactivate()</code></td>
    <td>Activator</td>
    <td>Allows the previous object to execute custom deactivation logic.</td>
  </tr>

  <tr class="success">
    <td><code>activate()</code></td>
    <td>Composition &amp; Activator</td>
    <td>Allows the new object to execute custom activation logic.</td>
  </tr>

  <tr class="success">
    <td><code>binding()</code></td>
    <td>Composition</td>
    <td>Notifies the new object immediately before databinding occurs.</td>
  </tr>

  <tr class="success">
    <td><code>bindingComplete()</code></td>
    <td>Composition</td>
    <td>Notifies the new object immediately after databinding occurs.</td>
  </tr>

  <tr class="success">
    <td><code>attached()</code></td>
    <td>Composition</td>
    <td>Notifies the new object when its view is attached to its parent DOM node.</td>
  </tr>

  <tr class="success">
    <td><code>compositionComplete()</code></td>
    <td>Composition</td>
    <td>Notifies the new object when the composition it participates in is complete.</td>
  </tr>

  <tr class="success">
    <td><code>detached()</code></td>
    <td>Composition</td>
    <td>Notifies a composed object when its view is removed from the DOM.</td>
  </tr>
</tbody></table>
```javascript
//app\hello\index.js
      system.log('Lifecycle : binding : hello');
            return { cacheViews:false }; //cancels view caching for this module, allowing the triggering of the detached callback
        },
        bindingComplete: function (view) {
            system.log('Lifecycle : bindingComplete : hello');
        },
        attached: function (view, parent) {
            system.log('Lifecycle : attached : hello');
        },
        compositionComplete: function (view,parent,context) {
            system.log('Lifecycle : compositionComplete : hello');
        },
        detached: function (view) {
            system.log('Lifecycle : detached : hello');
        }
```

## Documentation

All the documentation is located on [the official site](http://durandaljs.com/), so have a look there for help on how to [get started](http://durandaljs.com/get-started.html), [read tutorials](http://durandaljs.com/docs.html), [view sample descriptions](http://durandaljs.com/documentation/Understanding-the-Samples.html) and peruse the module reference docs.
- [downloads](http://durandaljs.com/downloads.html)


## install
```shell
npm install -g http-server
http-server -p 99
http://192.168.252.147:99/

```

## Community & Support

Need help with something that the docs aren't providing an answer to? 
Visit our [google group](https://groups.google.com/forum/?fromgroups#!forum/durandaljs) and join in the conversation.


 ## link
- [awesome-knockout](https://github.com/dnbard/awesome-knockout)
## Contributing

We'd love for you to contribute to the Durandal project! If you are interested, please have a read through our [contributing](/CONTRIBUTING.md) guide.

## License

The MIT License

Copyright (c) 2012 Blue Spire Consulting, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
